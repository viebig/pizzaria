using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class PagamentoDinheiro:Pagamento
    {
        private string formaPagamento = "Dinheiro";
        private double especie;

        public string FormaPagamento
        {
            get { return formaPagamento; }
            set { formaPagamento = value; }
        }

        public double Especie
        {
            get { return especie; }
            set { especie = value; }
        }
    }
}
