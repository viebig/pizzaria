using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class Pedido
    {
        private int id;
        private double total;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public double Total
        {
            get { return total; }
            set { total = value; }
        }
    }
}
