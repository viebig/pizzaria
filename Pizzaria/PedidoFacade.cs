using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class PedidoFacade
    {
        public void incluirPedido(Pedido pedido)
        {
            PedidoDAO pd = new PedidoDAO();
            pd.incluir(pedido);
        }

        public void incluirPedidoPizza(Pedido pedido, Pizza pizza, int quantidade)
        {

            PedidoPizza pp = new PedidoPizza();

            pp.Pedido = pedido;
            pp.Pizza = pizza;
            pp.Quantidade = quantidade;

            PedidoPizzaDAO pdp = new PedidoPizzaDAO();
            
            pdp.incluir(pp);
        }

        public List<Pedido> getPedidosAtivos()
        {

        }

    }
}
