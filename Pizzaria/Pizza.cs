using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class Pizza
    {
        private int id;
        private string nome;
        private double preco;
        private string status;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public double Preco
        {
            get { return preco; }
            set { preco = value; }
        }

        public double Preco
        {
            get { return preco; }
            set { preco = value; }
        }

        public double Status
        {
            get { return status; }
            set { status = value; }
        }
    }
}
