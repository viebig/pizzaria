using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class PagamentoCartao:Pagamento
    {
        private string numero;
        private string validade;
        private int codigo;

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Validade
        {
            get { return validade; }
            set { validade = value; }
        }

        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }
    }
}
