using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class PedidoPizza
    {
        private Pizza pizza;
        private Pedido pedido;
        private int quantidade;

        internal Pizza Pizza
        {
            get { return pizza; }
            set { pizza = value; }
        }

        internal Pedido Pedido
        {
            get { return pedido; }
            set { pedido = value; }
        }

        public int Quantidade
        {
            get { return quantidade; }
            set { quantidade = value; }
        }
    }
}
