using System;
using System.Collections.Generic;
using System.Text;

namespace Pizzaria
{
    class Pagamento
    {
        private int id;
        private double valor;
        private Pedido pedido;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public double Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        internal Pedido Pedido
        {
            get { return pedido; }
            set { pedido = value; }
        }
    }
}
